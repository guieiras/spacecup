package br.com.spacecup.form;

import br.com.spacecup.dao.AlunoDAO;
import br.com.spacecup.dao.GrupoDAO;
import br.com.spacecup.dao.LancamentoDAO;
import br.com.spacecup.modelo.Aluno;
import br.com.spacecup.modelo.Grupo;
import br.com.spacecup.modelo.Lancamento;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class FrmInfoGrupo extends javax.swing.JDialog {
    
    Grupo grupo;
    
    public FrmInfoGrupo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbNomeGrupo = new javax.swing.JLabel();
        lbNomeFoguete = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaParticipantes = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelaLancamentos = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Informações do Grupo");
        setResizable(false);

        lbNomeGrupo.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lbNomeGrupo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbNomeGrupo.setText("Nome do Grupo");

        lbNomeFoguete.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lbNomeFoguete.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbNomeFoguete.setText("Nome do Foguete");

        tabelaParticipantes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "RM", "Turma"
            }
        ));
        jScrollPane1.setViewportView(tabelaParticipantes);

        jLabel1.setText("Participantes:");

        jLabel2.setText("Lançamentos:");

        tabelaLancamentos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Data do Lançamento", "Distância até o Alvo", "Duração do voo"
            }
        ));
        jScrollPane2.setViewportView(tabelaLancamentos);

        jButton1.setForeground(new java.awt.Color(255, 0, 0));
        jButton1.setText("Excluir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Alterar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbNomeGrupo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbNomeFoguete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(lbNomeGrupo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNomeFoguete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        excluirGrupo();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        acessarFormEdicao();
    }//GEN-LAST:event_jButton2ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmInfoGrupo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmInfoGrupo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmInfoGrupo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmInfoGrupo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmInfoGrupo dialog = new FrmInfoGrupo(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    
    public void carregarFormulario(Grupo grupo) {
        Aluno aluno;
        Lancamento lancamento;
        String[][] matriz;
        TableModel modelo;
        
        this.grupo = grupo;
        lbNomeGrupo.setText(grupo.getNome());
        lbNomeFoguete.setText(grupo.getNomeFoguete());
        matriz = new String[grupo.getAlunos().size()][3];
        String[] colunasAluno = {"Nome", "RM", "Turma"};
        for (int i = 0; i < grupo.getAlunos().size(); i++) {
            aluno = grupo.getAlunos().get(i);
            matriz[i][0] = aluno.getNome();
            matriz[i][1] = Integer.toString(aluno.getRm());
            matriz[i][2] = aluno.getTurma();
        }
        modelo = new DefaultTableModel(matriz, colunasAluno) {
            @Override
            public boolean isCellEditable(int row, int column) {
               return false;
            }
        };
        tabelaParticipantes.setModel(modelo);
        
        matriz = new String[grupo.getLancamentos().size()][4];
        String[] colunasLancamento = {"ID", "Data de lançamento", "Distância até o alvo", "Duração do Voo"};
        
        for (int i = 0; i < grupo.getLancamentos().size(); i++) {
            lancamento = grupo.getLancamentos().get(i);
            matriz[i][0] = Integer.toString(lancamento.getId());
            matriz[i][1] = new SimpleDateFormat("dd/MM/yyyy").format(lancamento.getData());
            matriz[i][2] = Double.toString(lancamento.getDistanciaAlvo()) + " m";
            matriz[i][3] = Double.toString(lancamento.getDuracao()) + " s";
        }
        modelo = new DefaultTableModel(matriz, colunasLancamento) {
            @Override
            public boolean isCellEditable(int row, int column) {
               return false;
            } 
        };
        tabelaLancamentos.setModel(modelo);
    }
    
    public void acessarFormEdicao() {
        FrmEdicaoGrupo frm = new FrmEdicaoGrupo((FrmPrincipal) this.getParent(), true);
        frm.setLocationRelativeTo(this);
        frm.carregarGrupo(grupo);
        frm.setVisible(true);
        this.carregarFormulario(grupo);
    }
    
    public void excluirGrupo() {
        if (JOptionPane.showConfirmDialog(this, 
                "Tem certeza que deseja excluir o grupo? Essa ação não poderá ser desfeita.", 
                this.getTitle(), JOptionPane.YES_NO_OPTION) == 0) {
            try {
                new AlunoDAO().excluirAlunosDoGrupo(grupo.getId());
                new LancamentoDAO().excluirLancamentosDoFoguete(grupo.getIdFoguete());
                if(new GrupoDAO().excluirGrupo(grupo.getId())) {
                   JOptionPane.showMessageDialog(this, "Grupo excluído com sucesso!", this.getTitle(), 1);
                }
                else {
                   JOptionPane.showMessageDialog(this, "Não foi possível salvar o grupo", this.getTitle(), 0);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Erro ao excluir o grupo: " + e.getMessage(), this.getTitle(), 0);
            }
            this.dispose();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lbNomeFoguete;
    private javax.swing.JLabel lbNomeGrupo;
    private javax.swing.JTable tabelaLancamentos;
    private javax.swing.JTable tabelaParticipantes;
    // End of variables declaration//GEN-END:variables
}
