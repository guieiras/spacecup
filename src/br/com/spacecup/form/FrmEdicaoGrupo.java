package br.com.spacecup.form;

import br.com.spacecup.dao.AlunoDAO;
import br.com.spacecup.modelo.Grupo;
import br.com.spacecup.dao.GrupoDAO;
import br.com.spacecup.modelo.Aluno;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class FrmEdicaoGrupo extends javax.swing.JDialog {

    Grupo grupo;
    
    public FrmEdicaoGrupo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNomeGrupo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtNomeFoguete = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Gerenciar Grupo");
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Adicionar/Editar Grupo");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel2.setText("Nome do grupo:");

        jLabel3.setText("Nome do foguete:");

        jLabel4.setText("Participantes:");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RM", "Nome", "Turma"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jButton1.setText("+");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Salvar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("X");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNomeGrupo)
                            .addComponent(txtNomeFoguete)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton2)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNomeGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNomeFoguete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(jButton3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        salvarGrupo();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        abrirFormAluno(new Aluno());
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        desassociarAluno();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int linha = jTable1.getSelectedRow();
        if (evt.getClickCount() == 2 && linha != -1) {
            abrirFormAluno(grupo.getAlunos().get(linha));
        }
    }//GEN-LAST:event_jTable1MouseClicked

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmEdicaoGrupo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmEdicaoGrupo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmEdicaoGrupo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmEdicaoGrupo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmEdicaoGrupo dialog = new FrmEdicaoGrupo(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    
    public void abrirFormAluno(Aluno aluno) {
        String nomeGrupo = txtNomeGrupo.getText();
        String nomeFoguete = txtNomeFoguete.getText();
        aluno.setGrupoId(grupo.getId());
        FrmEdicaoAluno frm = new FrmEdicaoAluno((FrmPrincipal) this.getParent(), true);
        frm.setLocationRelativeTo(this);
        frm.carregarFormulario(aluno);
        frm.setVisible(true);
        if(aluno.getId() == 0) {
            grupo.getAlunos().add(aluno);
        }
        carregarGrupo(grupo);
        txtNomeGrupo.setText(nomeGrupo);
        txtNomeFoguete.setText(nomeFoguete);
    }
    
    public void carregarGrupo(Grupo grupo) {
        this.grupo = grupo;
        txtNomeGrupo.setText(grupo.getNome());
        txtNomeFoguete.setText(grupo.getNomeFoguete());
        String[][] matriz = new String[grupo.getAlunos().size()][3];

            Aluno aluno;
            String[] colunas = {"RM", "Nome", "Turma"};
            for (int i = 0; i < grupo.getAlunos().size(); i++) {
                aluno = grupo.getAlunos().get(i);
                matriz[i][0] = Integer.toString(aluno.getRm());
                matriz[i][1] = aluno.getNome();
                matriz[i][2] = aluno.getTurma();
            }
            TableModel modelo = new DefaultTableModel(matriz, colunas) {
                @Override
                public boolean isCellEditable(int row, int column) {
                   return false;
                }
            };
            jTable1.setModel(modelo);

    }
    
    public void salvarGrupo() {
        grupo.setNome(txtNomeGrupo.getText());
        grupo.setNomeFoguete(txtNomeFoguete.getText());
        try {
            if(grupo.getId() == 0) {
                if(new GrupoDAO().inserirGrupo(grupo)) {
                    JOptionPane.showMessageDialog(this, "Grupo cadastrado com sucesso!", this.getTitle(), 1);
                    for (Aluno aluno : grupo.getAlunos()) {
                        try {
                            new AlunoDAO().inserirAlunoNoUltimoGrupo(aluno);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this, "Falha ao salvar o aluno " + aluno.getNome() + ": " + e.getMessage(), this.getTitle(), 0);
                        }
                    }
                    this.dispose();
                }
                else {
                    JOptionPane.showMessageDialog(this, "Não foi possível cadastrar o grupo", this.getTitle(), 0);
                }
            }
            else {
                if(new GrupoDAO().alterarGrupo(grupo)) {
                    JOptionPane.showMessageDialog(this, "Grupo alterado com sucesso!", this.getTitle(), 1);
                    this.dispose();
                }
                else {
                    JOptionPane.showMessageDialog(this, "Não foi possível salvar o grupo", this.getTitle(), 0);
                }                
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Erro ao salvar o grupo: " + e.getMessage(), this.getTitle(), 0);
        }
    }
    
    public void desassociarAluno() {
        if (jTable1.getSelectedRow() != -1) {
            if (JOptionPane.showConfirmDialog(this,
                "Tem certeza que deseja excluir o aluno? Essa ação não poderá ser desfeita.",
                this.getTitle(), JOptionPane.YES_NO_OPTION) == 0) {
                try {
                    Aluno aluno = grupo.getAlunos().get(jTable1.getSelectedRow());
                    if (aluno.getId() != 0) {
                        if(new AlunoDAO().excluirAluno(aluno.getId())) {
                            grupo.getAlunos().remove(jTable1.getSelectedRow());
                            carregarGrupo(grupo);
                        }
                        else {
                            JOptionPane.showMessageDialog(this, "Não foi possível excluir o aluno", this.getTitle(), 0);
                        }
                    }
                    else {
                        if (grupo.getId() == 0) {
                            grupo.getAlunos().remove(jTable1.getSelectedRow());
                            carregarGrupo(grupo);
                        }
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, "Erro ao excluir o aluno: " + e.getMessage(), this.getTitle(), 0);
                }
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtNomeFoguete;
    private javax.swing.JTextField txtNomeGrupo;
    // End of variables declaration//GEN-END:variables
}
