package br.com.spacecup.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
    private static Connection connection;
    private static final String URL = "jdbc:oracle:thin:@oracle.fiap.com.br:1521:ORCL";
    private static final String USUARIO = "OPS$RM74395";
    private static final String SENHA = "220497";

    public static Connection getConnection() throws SQLException {
        if (connection == null) {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                connection = DriverManager.getConnection(URL, USUARIO, SENHA);
            }
            catch(ClassNotFoundException e) {}
        }
        return connection;
    }
}
