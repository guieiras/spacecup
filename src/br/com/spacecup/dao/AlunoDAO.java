package br.com.spacecup.dao;

import br.com.spacecup.conexao.Conexao;
import br.com.spacecup.modelo.Aluno;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AlunoDAO {
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public ArrayList<Aluno> listarAlunos(int grupoId) throws SQLException {
        Connection conexao = Conexao.getConnection();
        String sql = "SELECT * FROM aluno WHERE grupo_id = ?";

        preparedStatement = conexao.prepareStatement(sql);
        preparedStatement.setInt(1, grupoId);
        resultSet = preparedStatement.executeQuery();

        ArrayList<Aluno> lista = new ArrayList<>();
        int id, rm;
        String nome, turma;

        while (resultSet.next()) {
          id = resultSet.getInt("id");
          rm = resultSet.getInt("rm");
          nome = resultSet.getString("nome");
          turma = resultSet.getString("turma");
          lista.add(new Aluno(id, nome, rm, turma, grupoId));
        }

        return lista;
    }

    public Aluno obterAluno(int id) throws SQLException {
        Aluno aluno = null;
        int rm, idGrupo;
        String nome, turma;

        Connection conexao = Conexao.getConnection();
        String sql = "SELECT * FROM aluno WHERE id = ?";

        preparedStatement = conexao.prepareStatement(sql);
        preparedStatement.setInt(1, id);

        resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {
          id = resultSet.getInt("id");
          rm = resultSet.getInt("rm");
          nome = resultSet.getString("nome");
          turma = resultSet.getString("turma");
          idGrupo = resultSet.getInt("grupo_id");
          aluno = new Aluno(id, nome, rm, turma, idGrupo);
        }
        return aluno;
    }

    public boolean inserirAluno(Aluno aluno) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();

        sql = "INSERT INTO aluno VALUES (SEQ_ALUNO_ID.NEXTVAL, ?, ?, ?, ?)";
        preparedStatement = conexao.prepareStatement(sql);
        preparedStatement.setString(1, aluno.getNome());
        preparedStatement.setInt(2, aluno.getRm());
        preparedStatement.setString(3, aluno.getTurma());
        preparedStatement.setInt(4, aluno.getGrupoId());

        return preparedStatement.executeUpdate() > 0;
    }
    
    public boolean inserirAlunoNoUltimoGrupo(Aluno aluno) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();

        sql = "INSERT INTO aluno VALUES (SEQ_ALUNO_ID.NEXTVAL, ?, ?, ?, SEQ_GRUPO_ID.CURRVAL)";
        preparedStatement = conexao.prepareStatement(sql);
        preparedStatement.setString(1, aluno.getNome());
        preparedStatement.setInt(2, aluno.getRm());
        preparedStatement.setString(3, aluno.getTurma());

        return preparedStatement.executeUpdate() > 0;
    }

    public boolean alterarAluno(Aluno aluno) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();

        sql = "UPDATE aluno SET nome = ?, rm = ?, turma = ?, grupo_id = ? WHERE id = ?";
        preparedStatement = conexao.prepareStatement(sql);
        preparedStatement.setString(1, aluno.getNome());
        preparedStatement.setInt(2, aluno.getRm());
        preparedStatement.setString(3, aluno.getTurma());
        preparedStatement.setInt(4, aluno.getGrupoId());
        preparedStatement.setInt(5, aluno.getId());

        return preparedStatement.executeUpdate() > 0;
    }

    public boolean excluirAluno(int alunoId) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();
        if(alunoId == 0) {
            throw new Exception("Não é possível excluir o aluno porque ele "
                    + "não está persistido");
        }
        else {
            sql = "DELETE aluno WHERE id = ?";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setInt(1, alunoId);

            return preparedStatement.executeUpdate() > 0;
        }
    }

    public boolean excluirAlunosDoGrupo(int grupoId) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();
        if(grupoId == 0) {
            throw new Exception("Não é possível excluir os alunos porque o grupo "
                    + "informado não está persistido");
        }
        else {
            sql = "DELETE aluno WHERE grupo_id = ?";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setInt(1, grupoId);

            return preparedStatement.executeUpdate() > 0;
        }
    }
}
