package br.com.spacecup.dao;

import br.com.spacecup.conexao.Conexao;
import br.com.spacecup.modelo.Grupo;

import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GrupoDAO {
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public ArrayList<Grupo> listarGrupos() throws SQLException {
        Connection conexao = Conexao.getConnection();
        String sql = "SELECT Grupo.id, Grupo.nome, "
                + "Foguete.id AS id_foguete, Foguete.nome AS nome_foguete " +
          "FROM grupo INNER JOIN foguete ON grupo.foguete_id=foguete.id";

        preparedStatement = conexao.prepareStatement(sql);
        resultSet = preparedStatement.executeQuery();

        return gerarListaDeGrupos(resultSet);
    }

    public ArrayList<Grupo> filtrarGrupos(String nome) throws SQLException {
        Connection conexao = Conexao.getConnection();
        String sql = "SELECT Grupo.id, Grupo.nome, " +
          "Foguete.id AS id_foguete, Foguete.nome AS nome_foguete " +
          "FROM grupo INNER JOIN foguete ON grupo.foguete_id=foguete.id " +
          "WHERE grupo.nome LIKE ?";

        preparedStatement = conexao.prepareStatement(sql);
        preparedStatement.setString(1, '%' + nome + '%');

        resultSet = preparedStatement.executeQuery();

        return gerarListaDeGrupos(resultSet);
    }

    public Grupo obterGrupo(int id) throws SQLException {
        Grupo grupo = null;
        int idFoguete;
        String nome, nomeFoguete;
        Connection conexao = Conexao.getConnection();
        String sql = "SELECT grupo.id, grupo.nome, " +
          "foguete.id AS id_foguete, foguete.nome AS nome_foguete " +
          "FROM grupo INNER JOIN foguete ON grupo.foguete_id=foguete.id " +
          "WHERE grupo.id = ?";

        preparedStatement = conexao.prepareStatement(sql);
        preparedStatement.setInt(1, id);

        resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {
          id = resultSet.getInt("id");
          idFoguete = resultSet.getInt("id_foguete");
          nome = resultSet.getString("nome");
          nomeFoguete = resultSet.getString("nome_foguete");
          grupo = new Grupo(id, idFoguete, nome, nomeFoguete);
        }
        return grupo;
    }

    public boolean inserirGrupo(Grupo grupo) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();
        if(grupo.getNomeFoguete().isEmpty()) {
            throw new Exception("Não é possível salvar o grupo porque o "
                    + "foguete não foi informado");
        }
        else {
            sql = "INSERT INTO foguete (id,nome) VALUES (SEQ_FOGUETE_ID.NEXTVAL, ?)";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setString(1, grupo.getNomeFoguete());
            preparedStatement.execute();

            sql = "INSERT INTO grupo (id, nome, foguete_id) "
                    + "VALUES (SEQ_GRUPO_ID.NEXTVAL, ?, SEQ_FOGUETE_ID.CURRVAL)";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setString(1, grupo.getNome());

            return preparedStatement.executeUpdate() > 0;
        }
    }

    public boolean alterarGrupo(Grupo grupo) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();
        if(grupo.getId() == 0) {
            throw new Exception("Não é possível alterar o grupo porque ele "
                    + "não está persistido");
        }
        else {
            sql = "UPDATE foguete SET nome=? WHERE id=?";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setString(1, grupo.getNomeFoguete());
            preparedStatement.setInt(2, grupo.getIdFoguete());
            preparedStatement.execute();

            sql = "UPDATE grupo SET nome = ? WHERE id = ?";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setString(1, grupo.getNome());
            preparedStatement.setInt(2, grupo.getId());
            return preparedStatement.executeUpdate() > 0;
        }
    }

    public boolean excluirGrupo(int id) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();
        if(id == 0) {
            throw new Exception("Não é possível excluir o grupo porque ele "
                    + "não está persistido");
        }
        else {
            sql = "DELETE grupo WHERE id = ?";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            return preparedStatement.executeUpdate() > 0;
        }
    }

    private ArrayList<Grupo> gerarListaDeGrupos(ResultSet rs) throws SQLException {
        ArrayList<Grupo> lista = new ArrayList<>();
        int id, id_foguete;
        String nome, nomeFoguete;

        while (rs.next()) {
          id = rs.getInt("id");
          id_foguete = rs.getInt("id_foguete");
          nome = rs.getString("nome");
          nomeFoguete = rs.getString("nome_foguete");
          lista.add(new Grupo(id, id_foguete, nome, nomeFoguete));
        }

        return lista;
    }
}
