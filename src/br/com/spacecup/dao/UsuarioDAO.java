package br.com.spacecup.dao;

import br.com.spacecup.conexao.Conexao;
import br.com.spacecup.modelo.Usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioDAO {
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public boolean consultarUsuario(Usuario usuario) throws SQLException {
        Connection conexao = Conexao.getConnection();
        String sql = "SELECT * FROM usuario WHERE login = ? AND senha = ?";

        preparedStatement = conexao.prepareStatement(sql);
        preparedStatement.setString(1, usuario.getLogin());
        preparedStatement.setString(2, usuario.getSenha());

        resultSet = preparedStatement.executeQuery();
        return resultSet.next();
    }

    public boolean inserirUsuario(Usuario usuario) throws SQLException {
      Connection conexao = Conexao.getConnection();
      String sql = "INSERT INTO usuario VALUES (SEQ_USUARIO_ID.NEXTVAL, ?, ?)";

      preparedStatement = conexao.prepareStatement(sql);
      preparedStatement.setString(1, usuario.getLogin());
      preparedStatement.setString(2, usuario.getSenha());
      
      return preparedStatement.executeUpdate() > 0;
    }
}
