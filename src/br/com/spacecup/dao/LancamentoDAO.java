package br.com.spacecup.dao;

import br.com.spacecup.conexao.Conexao;
import br.com.spacecup.modelo.Grupo;
import br.com.spacecup.modelo.Lancamento;

import java.util.ArrayList;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LancamentoDAO {
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public ArrayList<Lancamento> listarLancamentos() throws SQLException {
        Connection conexao = Conexao.getConnection();
        String sql = "SELECT lancamento.id, lancamento.data, " +
            "lancamento.distancia_alvo, lancamento.angulo, " +
            "lancamento.velocidade_vento, lancamento.altitude_maxima, " +
            "lancamento.velocidade_maxima, lancamento.tempo_propulsao, " +
            "lancamento.pico_aceleracao, lancamento.aceleracao_media, " +
            "lancamento.tempo_apogeu_descida, lancamento.tempo_ejecao, " +
            "lancamento.altitude_ejecao, lancamento.taxa_descida, " +
            "lancamento.duracao_voo, foguete.peso, foguete.id AS id_foguete, " +
            "grupo.id AS id_grupo, grupo.nome AS nome_grupo " +
            "FROM LANCAMENTO " +
            "JOIN FOGUETE ON LANCAMENTO.FOGUETE_ID = FOGUETE.ID " +
            "JOIN GRUPO ON GRUPO.FOGUETE_ID = FOGUETE.ID";

        preparedStatement = conexao.prepareStatement(sql);
        resultSet = preparedStatement.executeQuery();

        return gerarListaDeLancamentos(resultSet);
    }

    public ArrayList<Lancamento> filtrarLancamentos(String nomeGrupo) throws SQLException {
        Connection conexao = Conexao.getConnection();
        String sql = "SELECT lancamento.id, lancamento.data, " +
            "lancamento.distancia_alvo, lancamento.angulo, " +
            "lancamento.velocidade_vento, lancamento.altitude_maxima, " +
            "lancamento.velocidade_maxima, lancamento.tempo_propulsao, " +
            "lancamento.pico_aceleracao, lancamento.aceleracao_media, " +
            "lancamento.tempo_apogeu_descida, lancamento.tempo_ejecao, " +
            "lancamento.altitude_ejecao, lancamento.taxa_descida, " +
            "lancamento.duracao_voo, foguete.peso, foguete.id AS id_foguete, " +
            "grupo.id AS id_grupo, grupo.nome AS nome_grupo " +
            "FROM LANCAMENTO " +
            "JOIN FOGUETE ON LANCAMENTO.FOGUETE_ID = FOGUETE.ID " +
            "JOIN GRUPO ON GRUPO.FOGUETE_ID = FOGUETE.ID " +
            "WHERE grupo.nome LIKE ?";

        preparedStatement = conexao.prepareStatement(sql);
        preparedStatement.setString(1, '%' + nomeGrupo + '%');

        resultSet = preparedStatement.executeQuery();

        return gerarListaDeLancamentos(resultSet);
    }
    
    public ArrayList<Lancamento> filtrarLancamentos(int idGrupo) throws SQLException {
        Connection conexao = Conexao.getConnection();
        String sql = "SELECT lancamento.id, lancamento.data, " +
            "lancamento.distancia_alvo, lancamento.angulo, " +
            "lancamento.velocidade_vento, lancamento.altitude_maxima, " +
            "lancamento.velocidade_maxima, lancamento.tempo_propulsao, " +
            "lancamento.pico_aceleracao, lancamento.aceleracao_media, " +
            "lancamento.tempo_apogeu_descida, lancamento.tempo_ejecao, " +
            "lancamento.altitude_ejecao, lancamento.taxa_descida, " +
            "lancamento.duracao_voo, foguete.peso, foguete.id AS id_foguete, " +
            "grupo.id AS id_grupo, grupo.nome AS nome_grupo " +
            "FROM LANCAMENTO " +
            "JOIN FOGUETE ON LANCAMENTO.FOGUETE_ID = FOGUETE.ID " +
            "JOIN GRUPO ON GRUPO.FOGUETE_ID = FOGUETE.ID " +
            "WHERE grupo.id = ?";

        preparedStatement = conexao.prepareStatement(sql);
        preparedStatement.setInt(1, idGrupo);

        resultSet = preparedStatement.executeQuery();

        return gerarListaDeLancamentos(resultSet);
    }

    public Lancamento obterLancamento(int id) throws SQLException {
        Lancamento lancamento = null;
        int idGrupo, idFoguete;
        String nomeGrupo;
        Date data;
        double distanciaAlvo, angulo, velocidadeVento, pesoFoguete, alturaMaxima,
        velocidadeMaxima, tempoPropulsao, picoAceleracao, aceleracaoMedia,
        tempoApogeuDescida, tempoEjecao, altitudeEjecao, taxaDescida, duracao;

        Connection conexao = Conexao.getConnection();
        String sql = "SELECT lancamento.id, lancamento.data, " +
            "lancamento.distancia_alvo, lancamento.angulo, " +
            "lancamento.velocidade_vento, lancamento.altitude_maxima, " +
            "lancamento.velocidade_maxima, lancamento.tempo_propulsao, " +
            "lancamento.pico_aceleracao, lancamento.aceleracao_media, " +
            "lancamento.tempo_apogeu_descida, lancamento.tempo_ejecao, " +
            "lancamento.altitude_ejecao, lancamento.taxa_descida, " +
            "lancamento.duracao_voo, foguete.peso, foguete.id AS id_foguete, " +
            "grupo.id AS id_grupo, grupo.nome AS nome_grupo " +
            "FROM LANCAMENTO " +
            "JOIN FOGUETE ON LANCAMENTO.FOGUETE_ID = FOGUETE.ID " +
            "JOIN GRUPO ON GRUPO.FOGUETE_ID = FOGUETE.ID " +
            "WHERE LANCAMENTO.ID = ?";

        preparedStatement = conexao.prepareStatement(sql);
        preparedStatement.setInt(1, id);

        resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {
            id = resultSet.getInt("id");
            idGrupo = resultSet.getInt("id_grupo");
            idFoguete = resultSet.getInt("id_foguete");
            nomeGrupo = resultSet.getString("nome_grupo");
            data = resultSet.getDate("data");
            distanciaAlvo = resultSet.getDouble("distancia_alvo");
            angulo = resultSet.getDouble("angulo");
            velocidadeVento = resultSet.getDouble("velocidade_vento");
            pesoFoguete = resultSet.getDouble("peso");
            alturaMaxima = resultSet.getDouble("altitude_maxima");
            velocidadeMaxima = resultSet.getDouble("velocidade_maxima");
            tempoPropulsao = resultSet.getDouble("tempo_propulsao");
            picoAceleracao = resultSet.getDouble("pico_aceleracao");
            aceleracaoMedia = resultSet.getDouble("aceleracao_media");
            tempoApogeuDescida = resultSet.getDouble("tempo_apogeu_descida");
            tempoEjecao = resultSet.getDouble("tempo_ejecao");
            altitudeEjecao = resultSet.getDouble("altitude_ejecao");
            taxaDescida = resultSet.getDouble("taxa_descida");
            duracao = resultSet.getDouble("duracao_voo");
            Grupo grupo = new Grupo(idGrupo, idFoguete, nomeGrupo, "");
            lancamento = new Lancamento(id, data, distanciaAlvo, angulo, velocidadeVento, pesoFoguete, alturaMaxima, velocidadeMaxima, tempoPropulsao, picoAceleracao, aceleracaoMedia, tempoApogeuDescida, tempoEjecao, altitudeEjecao, taxaDescida, duracao);
            lancamento.setGrupo(grupo);
        }
        return lancamento;
    }

    public boolean inserirLancamento(Lancamento lancamento) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();
        if(lancamento.getGrupo().getIdFoguete() <= 0) {
            throw new Exception("Não é possível salvar o lançamento porque o "
                    + "foguete não foi informado");
        }
        else {
            sql = "UPDATE FOGUETE SET PESO = ? WHERE ID = ?";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setDouble(1, lancamento.getPesoFoguete());
            preparedStatement.setInt(2, lancamento.getGrupo().getIdFoguete());
            preparedStatement.execute();

            sql = "INSERT INTO lancamento (id, data, angulo, velocidade_vento," +
                "altitude_maxima, velocidade_maxima, tempo_propulsao, pico_aceleracao," +
                "aceleracao_media, tempo_apogeu_descida, tempo_ejecao, altitude_ejecao," +
                "taxa_descida, duracao_voo, distancia_alvo, foguete_id) " +
                "VALUES (SEQ_LANCAMENTO_ID.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setDate(1, lancamento.getData());
            preparedStatement.setDouble(2, lancamento.getAngulo());
            preparedStatement.setDouble(3, lancamento.getVelocidadeVento());
            preparedStatement.setDouble(4, lancamento.getAlturaMaxima());
            preparedStatement.setDouble(5, lancamento.getVelocidadeMaxima());
            preparedStatement.setDouble(6, lancamento.getTempoPropulsao());
            preparedStatement.setDouble(7, lancamento.getPicoAceleracao());
            preparedStatement.setDouble(8, lancamento.getAceleracaoMedia());
            preparedStatement.setDouble(9, lancamento.getTempoApogeuDescida());
            preparedStatement.setDouble(10, lancamento.getTempoEjecao());
            preparedStatement.setDouble(11, lancamento.getAltitudeEjecao());
            preparedStatement.setDouble(12, lancamento.getTaxaDescida());
            preparedStatement.setDouble(13, lancamento.getDuracao());
            preparedStatement.setDouble(14, lancamento.getDistanciaAlvo());
            preparedStatement.setDouble(15, lancamento.getGrupo().getIdFoguete());

            return preparedStatement.executeUpdate() > 0;
        }
    }

    public boolean alterarLancamento(Lancamento lancamento) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();
        if(lancamento.getId() == 0) {
            throw new Exception("Não é possível alterar o grupo porque ele "
                    + "não está persistido");
        }
        else {
            sql = "UPDATE FOGUETE SET PESO = ? WHERE ID = ?";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setDouble(1, lancamento.getPesoFoguete());
            preparedStatement.setInt(2, lancamento.getGrupo().getIdFoguete());
            preparedStatement.execute();

            sql = "UPDATE lancamento SET data = ?, angulo = ?, velocidade_vento = ?," +
                "altitude_maxima = ?, velocidade_maxima = ?, tempo_propulsao = ?, pico_aceleracao = ?," +
                "aceleracao_media = ?, tempo_apogeu_descida = ?, tempo_ejecao = ?, altitude_ejecao = ?," +
                "taxa_descida = ?, distancia_alvo = ?, duracao_voo = ? WHERE ID = ?";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setDate(1, lancamento.getData());
            preparedStatement.setDouble(2, lancamento.getAngulo());
            preparedStatement.setDouble(3, lancamento.getVelocidadeVento());
            preparedStatement.setDouble(4, lancamento.getAlturaMaxima());
            preparedStatement.setDouble(5, lancamento.getVelocidadeMaxima());
            preparedStatement.setDouble(6, lancamento.getTempoPropulsao());
            preparedStatement.setDouble(7, lancamento.getPicoAceleracao());
            preparedStatement.setDouble(8, lancamento.getAceleracaoMedia());
            preparedStatement.setDouble(9, lancamento.getTempoApogeuDescida());
            preparedStatement.setDouble(10, lancamento.getTempoEjecao());
            preparedStatement.setDouble(11, lancamento.getAltitudeEjecao());
            preparedStatement.setDouble(12, lancamento.getTaxaDescida());
            preparedStatement.setDouble(13, lancamento.getDuracao());
            preparedStatement.setDouble(14, lancamento.getDistanciaAlvo());
            preparedStatement.setDouble(15, lancamento.getId());

            return preparedStatement.executeUpdate() > 0;
        }
    }

    public boolean excluirLancamento(int lancamentoId) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();
        if(lancamentoId == 0) {
            throw new Exception("Não é possível excluir o grupo porque ele "
                    + "não está persistido");
        }
        else {
            sql = "DELETE lancamento WHERE id = ?";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setInt(1, lancamentoId);

            return preparedStatement.executeUpdate() > 0;
        }
    }
    
    public boolean excluirLancamentosDoFoguete(int fogueteId) throws Exception, SQLException {
        String sql;
        Connection conexao = Conexao.getConnection();
        if(fogueteId == 0) {
            throw new Exception("Não é possível excluir os lançamentos"
                    + "pois o grupo não está persistido");
        }
        else {
            sql = "DELETE lancamento WHERE foguete_id = ?";
            preparedStatement = conexao.prepareStatement(sql);
            preparedStatement.setInt(1, fogueteId);

            return preparedStatement.executeUpdate() > 0;
        }
    }

    private ArrayList<Lancamento> gerarListaDeLancamentos(ResultSet rs) throws SQLException {
        ArrayList<Lancamento> lista = new ArrayList<>();
        Lancamento lancamento;
        Grupo grupo;
        int id, idGrupo, idFoguete;
        String nomeGrupo;
        Date data;
        double distanciaAlvo, angulo, velocidadeVento, pesoFoguete, alturaMaxima,
        velocidadeMaxima, tempoPropulsao, aceleracaoMedia, tempoApogeuDescida,
        picoAceleracao, tempoEjecao, altitudeEjecao, taxaDescida, duracao;

        while (rs.next()) {
            id = resultSet.getInt("id");
            idGrupo = resultSet.getInt("id_grupo");
            idFoguete = resultSet.getInt("id_foguete");
            nomeGrupo = resultSet.getString("nome_grupo");
            data = resultSet.getDate("data");
            distanciaAlvo = resultSet.getDouble("distancia_alvo");
            angulo = resultSet.getDouble("angulo");
            velocidadeVento = resultSet.getDouble("velocidade_vento");
            pesoFoguete = resultSet.getDouble("peso");
            alturaMaxima = resultSet.getDouble("altitude_maxima");
            velocidadeMaxima = resultSet.getDouble("velocidade_maxima");
            tempoPropulsao = resultSet.getDouble("tempo_propulsao");
            picoAceleracao = resultSet.getDouble("pico_aceleracao");
            aceleracaoMedia = resultSet.getDouble("aceleracao_media");
            tempoApogeuDescida = resultSet.getDouble("tempo_apogeu_descida");
            tempoEjecao = resultSet.getDouble("tempo_ejecao");
            altitudeEjecao = resultSet.getDouble("altitude_ejecao");
            taxaDescida = resultSet.getDouble("taxa_descida");
            duracao = resultSet.getDouble("duracao_voo");
            grupo = new Grupo(idGrupo, idFoguete, nomeGrupo, "");
            lancamento = new Lancamento(id, data, distanciaAlvo, angulo, velocidadeVento, pesoFoguete, alturaMaxima, velocidadeMaxima, tempoPropulsao, picoAceleracao, aceleracaoMedia, tempoApogeuDescida, tempoEjecao, altitudeEjecao, taxaDescida, duracao);
            lancamento.setGrupo(grupo);
            lista.add(lancamento);
        }

        return lista;
    }
}
