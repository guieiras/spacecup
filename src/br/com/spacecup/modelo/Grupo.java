package br.com.spacecup.modelo;

import java.util.ArrayList;

public class Grupo {
    private int id;
    private int idFoguete;
    private String nome;
    private String nomeFoguete;
    private ArrayList<Aluno> alunos = new ArrayList();
    private ArrayList<Lancamento> lancamentos = new ArrayList();

    public Grupo() {}

    public Grupo(int id, int idFoguete, String nome, String nomeFoguete) {
        this.id = id;
        this.idFoguete = idFoguete;
        this.nome = nome;
        this.nomeFoguete = nomeFoguete;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdFoguete() {
        return idFoguete;
    }

    public void setIdFoguete(int idFoguete) {
        this.idFoguete = idFoguete;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeFoguete() {
        return nomeFoguete;
    }

    public void setNomeFoguete(String nomeFoguete) {
        this.nomeFoguete = nomeFoguete;
    }

    public ArrayList<Aluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(ArrayList<Aluno> alunos) {
        this.alunos = alunos;
    }

    public ArrayList<Lancamento> getLancamentos() {
        return lancamentos;
    }

    public void setLancamentos(ArrayList<Lancamento> lancamentos) {
        this.lancamentos = lancamentos;
    }
}
