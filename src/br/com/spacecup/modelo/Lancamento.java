package br.com.spacecup.modelo;
import java.sql.Date;

public class Lancamento {
    private int id;
    private Grupo grupo;
    private Date data;
    private double distanciaAlvo;
    private double angulo;
    private double velocidadeVento;
    private double pesoFoguete;
    private double alturaMaxima;
    private double velocidadeMaxima;
    private double tempoPropulsao;
    private double picoAceleracao;
    private double aceleracaoMedia;
    private double tempoApogeuDescida;
    private double tempoEjecao;
    private double altitudeEjecao;
    private double taxaDescida;
    private double duracao;

    public Lancamento() {}

    public Lancamento(int id, Date data, double distanciaAlvo, double angulo, double velocidadeVento, double pesoFoguete, double alturaMaxima, double velocidadeMaxima, double tempoPropulsao, double picoAceleracao, double aceleracaoMedia, double tempoApogeuDescida, double tempoEjecao, double altitudeEjecao, double taxaDescida, double duracao) {
        this.id = id;
        this.data = data;
        this.distanciaAlvo = distanciaAlvo;
        this.angulo = angulo;
        this.velocidadeVento = velocidadeVento;
        this.pesoFoguete = pesoFoguete;
        this.alturaMaxima = alturaMaxima;
        this.velocidadeMaxima = velocidadeMaxima;
        this.tempoPropulsao = tempoPropulsao;
        this.picoAceleracao = picoAceleracao;
        this.aceleracaoMedia = aceleracaoMedia;
        this.tempoApogeuDescida = tempoApogeuDescida;
        this.tempoEjecao = tempoEjecao;
        this.altitudeEjecao = altitudeEjecao;
        this.taxaDescida = taxaDescida;
        this.duracao = duracao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public double getDistanciaAlvo() {
        return distanciaAlvo;
    }

    public void setDistanciaAlvo(double distanciaAlvo) {
        this.distanciaAlvo = distanciaAlvo;
    }

    public double getAngulo() {
        return angulo;
    }

    public void setAngulo(double angulo) {
        this.angulo = angulo;
    }

    public double getVelocidadeVento() {
        return velocidadeVento;
    }

    public void setVelocidadeVento(double velocidadeVento) {
        this.velocidadeVento = velocidadeVento;
    }

    public double getPesoFoguete() {
        return pesoFoguete;
    }

    public void setPesoFoguete(double pesoFoguete) {
        this.pesoFoguete = pesoFoguete;
    }

    public double getAlturaMaxima() {
        return alturaMaxima;
    }

    public void setAlturaMaxima(double alturaMaxima) {
        this.alturaMaxima = alturaMaxima;
    }

    public double getVelocidadeMaxima() {
        return velocidadeMaxima;
    }

    public void setVelocidadeMaxima(double velocidadeMaxima) {
        this.velocidadeMaxima = velocidadeMaxima;
    }

    public double getTempoPropulsao() {
        return tempoPropulsao;
    }

    public void setTempoPropulsao(double tempoPropulsao) {
        this.tempoPropulsao = tempoPropulsao;
    }

    public double getPicoAceleracao() {
        return picoAceleracao;
    }

    public void setPicoAceleracao(double picoAceleracao) {
        this.picoAceleracao = picoAceleracao;
    }

    public double getAceleracaoMedia() {
        return aceleracaoMedia;
    }

    public void setAceleracaoMedia(double aceleracaoMedia) {
        this.aceleracaoMedia = aceleracaoMedia;
    }

    public double getTempoApogeuDescida() {
        return tempoApogeuDescida;
    }

    public void setTempoApogeuDescida(double tempoApogeuDescida) {
        this.tempoApogeuDescida = tempoApogeuDescida;
    }

    public double getTempoEjecao() {
        return tempoEjecao;
    }

    public void setTempoEjecao(double tempoEjecao) {
        this.tempoEjecao = tempoEjecao;
    }

    public double getAltitudeEjecao() {
        return altitudeEjecao;
    }

    public void setAltitudeEjecao(double altitudeEjecao) {
        this.altitudeEjecao = altitudeEjecao;
    }

    public double getTaxaDescida() {
        return taxaDescida;
    }

    public void setTaxaDescida(double taxaDescida) {
        this.taxaDescida = taxaDescida;
    }

    public double getDuracao() {
        return duracao;
    }

    public void setDuracao(double duracao) {
        this.duracao = duracao;
    }
}
