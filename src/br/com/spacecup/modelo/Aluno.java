package br.com.spacecup.modelo;

public class Aluno {
   private int id;
   private String nome;
   private int rm;
   private String turma;
   private int grupoId;

    public Aluno() {}

    public Aluno(int id, String nome, int rm, String turma, int grupoId) {
        this.id = id;
        this.nome = nome;
        this.rm = rm;
        this.turma = turma;
        this.grupoId = grupoId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(int grupoId) {
        this.grupoId = grupoId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getRm() {
        return rm;
    }

    public void setRm(int rm) {
        this.rm = rm;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }
}
